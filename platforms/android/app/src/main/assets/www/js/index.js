var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    db: {},
    user: {},
    areas: {},
    tasks: {},
    reports: {},
    contacts: {},
    pingIntervalId: 0,

    //url: 'http://mala.sytes.net',
    url: 'https://olympus.iosapien.com',
    dbfile: 'olympusapp.json',

    goToDash: function() {
        $('#login').hide();
        $('#dash').show();
        $('.menudropdown').show();
        $('#coordFirstName').text(app.user.get(1).firstname);

        var oldProvince = oldCity = '';
        $.each(app.areas.data, function(index, obj){
            if (oldProvince != obj.ProvinceCode) {
                $('#province').append('<option value="' + obj.ProvinceCode + '">' + obj.ProvinceName + '</option>');
                oldProvince = obj.ProvinceCode;
            }
            if (oldCity != obj.MunicipalityCode) {
                $('#city').append('<option value="' + obj.MunicipalityCode + '" class="'+ obj.ProvinceCode +'">' + obj.MunicipalityName + '</option>');
                oldCity = obj.MunicipalityCode;
            }

        });

        $('#province').on('change', function(e){
            $('#city').children('option').hide();
            $('#city').children('option.' + $(this).val()).show();
        });

        //$('#city').on('change', function(e){
        //    console.log('hiding barangay');
        //    $('#barangay').children('option').hide();
        //    $('#barangay').children('option.mc_' + $(this).val()).show();
        //});

        $.each(app.tasks.data, function(index, obj){
            $('#task').append('<option value="' + obj.id + '">' + obj.name + '</option>');
        });

    },

    onDeviceReady: function() {

        app.db = new loki(app.dbfile, {
            autosave: true,
            autosaveInterval: 1000,
            autoload: true
        });

        app.db.loadDatabase({}, function(){
            app.user = app.db.getCollection('user');
            app.areas = app.db.getCollection('areas');
            app.tasks = app.db.getCollection('tasks');
            app.contacts = app.db.getCollection('contacts');
            app.reports = app.db.getCollection('reports');
            console.log(app.user);
            console.log(app.contacts);

            if (app.contacts != null) {
                app.resetContactQueue();
            }

            if (app.reports != null) {
                $('#reportcount').html( app.reports.data.length );
            }

            if (app.user != null) {
                app.goToDash();
            }
        });

        
        $('#btnLogin').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                url: app.url + '/api/creds',
                crossDomain: true,
                data: {
                    'user': $('#username').val(), 'pass': $('#password').val()
                },
                dataType: 'json',
                method: 'POST'
            }).done(function (res) {

                if (res.success == 'false') {
                    alert('Invalid Login');
                } else {
                    app.tasks = app.db.addCollection('tasks', {indices: ['name', 'id']});
                    app.user = app.db.addCollection('user', {indices: ['email']});
                    app.areas = app.db.addCollection('areas', {indices: ['BarangayCode', 'RegionCode', 'ProvinceCode', 'MunicipalityCode']});
                    app.contacts = app.db.addCollection('contacts');
                    app.reports = app.db.addCollection('reports');

                    app.user.insert(res.user);
                    app.areas.insert(res.areas);
                    app.tasks.insert(res.tasks);

                    app.db.saveDatabase();
                    app.goToDash();
                }

            });
            
        });

        $('.menubtn').on('click', function(e){
            e.preventDefault();
            //hide all panels
            $('.app-panel').hide();
            $('.responses').hide();
            $('.status-spinner').hide();

            app.resetReportQueue();
            app.resetContactQueue();
            //show selected panel
            $('#' + $(this).val()).show();
        });

        $('#mainicon').on('click', function(e){
            e.preventDefault();
            if ($('#login').is(':visible')) {
                //
            }else{
                $('.app-panel').hide();
                $('#dash').show();
            }

        });

        $('#btnCamera').on('click', function(e) {
            e.preventDefault();
            navigator.camera.getPicture(app.onCameraSuccess, app.onCameraFail, { quality: 50,
                destinationType: Camera.DestinationType.FILE_URI
            });
        });

        $('#btnSubmitReport').on('click', function(e) {
            e.preventDefault();
            $('#reportspinner').show();

            var params = {};
            params.barangay = $('#barangay').val();
            params.streetAddress = $('#streetAddress').val();
            params.task_id = $('#task').val();
            params.activityRoute = $('#activityRoute').val();
            params.feedback = $('#feedback').val();
            params.caption = $('#caption').val();
            params.coordinatorId = app.user.get(1).id;
            params.latitude = $('#latitude').html();
            params.longitude = $('#longitude').html();
            params.photoFile = $('#imgIncident').attr('src');


            app.reports = app.db.getCollection('reports');

            if (app.reports == null) {
                app.reports = app.db.addCollection('reports');
            }

            app.reports.insert(params);
            app.db.saveDatabase();

            app.resetForm();
            app.resetReportQueue();

            $('#reportspinner').hide();
            $('#report-response').html('');

            if (confirm('Report Submitted. Create another?')) {
                $('#province').focus();
            } else {
                $('.app-panel').hide();
                $('#view-report').show();
            }

        });

        $('#btnSubmitContact').on('click', function(e) {
            e.preventDefault();
            $('#contactspinner').show();
            $('#contact-response').html('Saving contact...');

            var params = {};
            params.firstname = $('#firstname').val();
            params.middlename = $('#middlename').val();
            params.lastname = $('#lastname').val();
            params.gender = $('#gender').val();
            params.birthday = $('#birthday').val();
            params.mobile = $('#mobile').val();
            params.email = $('#email').val();
            params.remarks = $('#remarks').val();
            params.coordinatorId = app.user.get(1).id;

            app.contacts = app.db.getCollection('contacts');
            if (app.contacts == null) {
                app.contacts = app.db.addCollection('contacts');
            }

            app.contacts.insert(params);
            app.db.saveDatabase();
            app.resetForm();
            app.resetContactQueue();
            $('#contactspinner').hide();
            $('#contact-response').html('');


            if (confirm('Contact Submitted. Create another?')) {
                $('#gender').focus();
            } else {
                $('.app-panel').hide();
                $('#view-contact').show();
            }

        });

        $('#btnUpReports').on('click', function(){
            $('#reportQueueMessage').html('Uploading reports in queue.');
            $('#reportQueueSpinner').show();
            app.pushReport();
        });

        $('#btnUpContacts').on('click', function(){
            $('#contactQueueMessage').html('Uploading contacts in queue.');
            $('#contactQueueSpinner').show();
            app.pushContact();
        });

        $('#btnGetGeo').on('click', function(e) {
            e.preventDefault();
            $('#geospinner').show();
            navigator.geolocation.getCurrentPosition(app.geolocationSuccess, app.geolocationFail, { timeout: 60000, maximumAge:0, enableHighAccuracy: true });
        });

        $('#btnExit').on('click', function(e) {
            e.preventDefault();
            if ( confirm('Exit the app?') ){
                app.db.saveDatabase();
                app.db.close();
                app.user = null;
                navigator.app.exitApp();
            }
        });

        $('#switch-ping').on('click', function(e){

            if ($(this).is(':checked')) {
                app.ping();
                app.pingIntervalId = window.setInterval(app.ping, 60000);
            } else {
                window.clearInterval(app.pingIntervalId);
            }

        });

        navigator.geolocation.getCurrentPosition(app.geolocationSuccess, app.geolocationFail, { timeout: 60000, maximumAge:0, enableHighAccuracy: true });
    },

    ping: function() {
        console.log('Ping locator!');
        navigator.geolocation.getCurrentPosition(
            function(position){
                $.post(
                    app.url + '/api/locator',
                    {
                        coordinatorId: app.user.get(1).id,
                        longitude: position.coords.longitude,
                        latitude: position.coords.latitude
                    },
                    function(resp) {
                        console.log(resp);
                    }
                );
            },
            function(){
                console.log('Error posting locator');
            },
            { timeout: 10000, maximumAge:0, enableHighAccuracy: true }
        );
    },

    resetForm: function() {
        $('input[type=text]').val('');
        $('input[type=email]').val('');
        $('input[type=number]').val('');
        $('input[type=date]').val('');
        $('input[type=textarea]').val('');
        $('#imgIncident').attr('src', '');
        $('select').val('');
        $('div').removeClass('is-dirty');
    },

    resetReportQueue: function() {
        $('#reportcount').html( app.reports.data.length );
    },

    resetContactQueue: function() {
        $('#contactcount').html( app.contacts.data.length );
    },

    pushContact: function() {

        $('#contactQueuespinner').show();
        $('#contactQueueResponse').html('Uploading queued contacts...');

        if (app.contacts != null && app.contacts.data.length > 0) {
            var win = function (r) {
                console.log(r);
                app.db.loadDatabase({}, function() {

                    app.contacts.remove(app.contacts.data[0]);

                    if (app.contacts.data.length == 0) {
                        app.db.removeCollection('contacts');
                        app.db.addCollection('contacts');

                        $('#contactQueueResponse').html('Submission successful.');
                        $('#contactQueuespinner').hide();

                    }
                    app.db.save();
                    app.db.saveDatabase();

                    app.resetContactQueue();
                    app.pushContact();
                });


            }

            var fail = function (error) {
                console.log(error);
                app.db.loadDatabase({}, function() {
                    $('#contactQueueResponse').html('An error occured while uploading.');

                    if (app.contacts.data.length == 0) {
                        app.db.removeCollection('contacts');
                        app.db.addCollection('contacts');
                        $('#contactQueuespinner').hide();
                    }
                    app.db.save();
                    app.db.saveDatabase();

                    app.resetContactQueue();
                });


            }

            var params = app.contacts.data[0];

            $.ajax({
                url: app.url + '/api/contact',
                crossDomain: true,
                data: params,
                dataType: 'json',
                method: 'POST'
            }).done(function (res) {
                win(res);
            }).error(function(res){
                fail(res);
            });

        } else {
            $('#contactQueueResponse').html('No contacts found in queue.');
            $('#contactQueueSpinner').hide();

        }

    },

    resetReport: function(message) {
        $('#reportcount').html(app.reports.data.length);
        $('#reportQueueMessage').html(message);
        $('#reportQueueSpinner').hide();
    },

    pushReport: function() {

        $('#reportQueueMessage').html('Starting report upload...');
        $('#reportQueueSpinner').show();

        var win = function (r) {
            console.log(r);
            app.db.loadDatabase({}, function() {
                console.log(app.reports.data);
                app.reports.remove(app.reports.data[0]);

                if (app.reports.data.length == 0) {
                    app.db.removeCollection('reports');
                    app.db.addCollection('reports');
                    app.resetReport('Thank you. Your report has been submitted.');
                }

                app.db.save();
                app.db.saveDatabase();
                app.pushReport();
            });

        }

        var fail = function (error) {
            console.log(error);
            app.db.loadDatabase({}, function() {

                if (app.reports.data.length == 0) {
                    app.db.removeCollection('reports');
                    app.db.addCollection('reports');
                    app.db.save();
                    app.db.saveDatabase();
                }

                app.resetReport('An error occurred while uploading.');
                app.resetReportQueue();
            });

        }

        if (app.reports != null && app.reports.data.length > 0) {

            params = app.reports.data[0];
            var fileURL = params.photoFile;
            var options = new FileUploadOptions();
            options.fileKey = "photo";
            options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            options.httpMethod= "POST";
            options.trustAllHosts = true;

            //var params = {};
            //params.barangay = $('#barangay').val();
            //params.streetAddress = $('#streetAddress').val();
            //params.task_id = $('#task').val();
            //params.activityRoute = $('#activityRoute').val();
            //params.feedback = $('#feedback').val();
            //params.caption = $('#caption').val();
            //params.coordinatorId = app.user.get(1).id;
            //params.latitude = $('#latitude').html();
            //params.longitude = $('#longitude').html();

            options.params = params;
            console.log(options);
            var ft = new FileTransfer();
            ft.upload(fileURL, encodeURI(app.url + "/api/report"), win, fail, options);
        } else {
            $('#reportQueueMessage').html('No reports found in queue');
            $('#reportQueueSpinner').hide();
        }


    },


    geolocationSuccess: function(position) {
        $('#longitude').html(position.coords.longitude);
        $('#latitude').html(position.coords.latitude);
        // $('#btnGetGeo').hide();
        $('#geospinner').hide();
    },

    geolocationFail: function(err) {

        alert('Location could not be determined. Please make sure GPS is on then click Geolocation. ' + err.message);
        $('#geospinner').hide();
    },

    onCameraSuccess: function(imageURI) {
        var image = document.getElementById('imgIncident');
        image.src = imageURI;
    },

    onCameraFail: function(message) {
        console.log(message);
        alert('Camera could not be initialized.');
    }
};

app.initialize();